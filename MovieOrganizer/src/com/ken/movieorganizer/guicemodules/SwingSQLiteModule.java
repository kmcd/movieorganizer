package com.ken.movieorganizer.guicemodules;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.stereotype.Service;

import com.google.inject.AbstractModule;
import com.ken.movieorganizer.gui.view.AddMovie;
import com.ken.movieorganizer.gui.view.ConfirmUpdate;
import com.ken.movieorganizer.gui.view.DeleteMovie;
import com.ken.movieorganizer.gui.view.EditChangeMovie;
import com.ken.movieorganizer.gui.view.EditSelectMovie;
import com.ken.movieorganizer.gui.view.MainGUI;
import com.ken.movieorganizer.gui.view.swing.SwingAddMovie;
import com.ken.movieorganizer.gui.view.swing.SwingConfirmUpdate;
import com.ken.movieorganizer.gui.view.swing.SwingDeleteMovie;
import com.ken.movieorganizer.gui.view.swing.SwingEditSelectMovie;
import com.ken.movieorganizer.gui.view.swing.SwingEditChangeMovie;
import com.ken.movieorganizer.gui.view.swing.SwingGUI;
import com.ken.movieorganizer.storage.MovieStorage;
import com.ken.movieorganizer.storage.sqlite.SQLiteMovieStorage;


public class SwingSQLiteModule extends AbstractModule{	

	/**
	 * A guice module that maps the interface's to their
	 * respective view classes.
	 * 
	 * Specific for using Swing/SQLite
	 * 
	 */
	@Override
	protected void configure() {
	
		Statement statement = null;
			
		try {
			
			 Class.forName("org.sqlite.JDBC");
			
			Connection connection = DriverManager.getConnection("jdbc:sqlite:sqlschema/00initialcreation");
			statement = connection.createStatement();
		    statement.setQueryTimeout(30);
		    
		        	
		
		} catch (SQLException e) {
			
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
			
	
		
		bind(AddMovie.class).to(SwingAddMovie.class);
		bind(ConfirmUpdate.class).to(SwingConfirmUpdate.class);
		bind(DeleteMovie.class).to(SwingDeleteMovie.class);
		bind(EditChangeMovie.class).to(SwingEditChangeMovie.class);
		bind(EditSelectMovie.class).to(SwingEditSelectMovie.class);			
		bind(Statement.class).toInstance(statement);
		bind(MovieStorage.class).to(SQLiteMovieStorage.class);
		bind(MainGUI.class).to(SwingGUI.class);				
		
	}

}