package com.ken.movieorganizer.guicemodules;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Morphia;
import com.google.inject.AbstractModule;
import com.ken.movieorganizer.Movie;
import com.ken.movieorganizer.gui.view.AddMovie;
import com.ken.movieorganizer.gui.view.ConfirmUpdate;
import com.ken.movieorganizer.gui.view.DeleteMovie;
import com.ken.movieorganizer.gui.view.EditChangeMovie;
import com.ken.movieorganizer.gui.view.EditSelectMovie;
import com.ken.movieorganizer.gui.view.MainGUI;
import com.ken.movieorganizer.gui.view.swing.SwingAddMovie;
import com.ken.movieorganizer.gui.view.swing.SwingConfirmUpdate;
import com.ken.movieorganizer.gui.view.swing.SwingDeleteMovie;
import com.ken.movieorganizer.gui.view.swing.SwingEditSelectMovie;
import com.ken.movieorganizer.gui.view.swing.SwingEditChangeMovie;
import com.ken.movieorganizer.gui.view.swing.SwingGUI;
import com.ken.movieorganizer.storage.MovieStorage;
import com.ken.movieorganizer.storage.mongo.MongoMovieStorage;
import com.mongodb.MongoClient;

public class SwingMongoModule extends AbstractModule{	

	/**
	 * A guice module that maps the interface's to their
	 * respective view classes.
	 * 
	 * Specific for using Swing/MongoDB
	 * 
	 */
	@Override
	protected void configure() {
	
		String userHome = System.getProperty("user.home");  
		String file = userHome + "/MovieOrganizer/Server.txt";
		
		Datastore datastore = null;
		
		try{
			
			InputStream inputStream = new FileInputStream(file);
			BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));
			
			String line1 = buffer.readLine();
			String line2 = buffer.readLine();
			String line3 = buffer.readLine();
											
			buffer.close();
			
			MongoClient mongoClient = new MongoClient( "ds041208.mongolab.com" , 41208 );
			Morphia morphia = new Morphia();		
			morphia.map(Movie.class);
			datastore = morphia.createDatastore(mongoClient, line1, line2, line3.toCharArray());
						
		} 
		
		catch (IOException e) {
			
			e.printStackTrace();
			
		}
		
		bind(AddMovie.class).to(SwingAddMovie.class);
		bind(ConfirmUpdate.class).to(SwingConfirmUpdate.class);
		bind(DeleteMovie.class).to(SwingDeleteMovie.class);
		bind(EditChangeMovie.class).to(SwingEditChangeMovie.class);
		bind(EditSelectMovie.class).to(SwingEditSelectMovie.class);			
		bind(Datastore.class).toInstance(datastore);
		bind(MovieStorage.class).to(MongoMovieStorage.class);
		bind(MainGUI.class).to(SwingGUI.class);				
		
	}

}