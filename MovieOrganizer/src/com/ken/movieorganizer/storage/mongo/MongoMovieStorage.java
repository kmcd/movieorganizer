package com.ken.movieorganizer.storage.mongo;

import java.util.ArrayList;
import java.util.List;
import com.google.code.morphia.Datastore;
import com.google.code.morphia.query.Query;
import com.google.code.morphia.query.UpdateOperations;
import com.google.inject.Inject;
import com.ken.movieorganizer.Movie;
import com.ken.movieorganizer.storage.MovieStorage;

/**
 *  A Mongo specific implementation of MovieStorage.java
 *  
 *  A Mongo server will need to be set up to use this class.
 *  A free server is available at www.mongolab.com.
 *  Also a file at user.home/MovieOrganizer/Server.txt will need to be made. 
 *  
 *  It should be in the format...
 *  
 *  Database Name
 *  User
 *  Password
 *  
 *  Also the host and port will need to be changed to the 
 *  correct String and int respective to the server.
 *    
 *
 *
 */
public class MongoMovieStorage implements MovieStorage {
	
	Datastore datastore;
	
	//@Inject
	public MongoMovieStorage(Datastore datastore){
		
		this.datastore = datastore;
		
	}
	
	@Override
	public List<String> getList(){
		
		List<Movie> listMovie;
		List<String> getList = new ArrayList<String>();								
		
		datastore.find(Movie.class).retrievedFields( true, "title").asList();
		
		Query<Movie> query = datastore.createQuery(Movie.class).order("title");
		
		listMovie = query.asList();
	
		for (Movie movie: listMovie){
			
			getList.add(movie.getTitle());				
			
		}			
				
		return getList;
	}
	
	@Override
	public List<Movie> getAll(){
		
		List<Movie> listMovie = null;				
									
		Query<Movie> query = datastore.createQuery(Movie.class).order("title");
					
		listMovie = query.asList();	
		
		return listMovie;
		
	}
	
	@Override
	public void create(Movie movie){			
									
		datastore.save(movie);			
		
	}
	
	@Override
	public void update(Movie movie){
		
		UpdateOperations<Movie> ops;
		
		Query<Movie> updateQuery = datastore.createQuery(Movie.class).field("_id").equal(movie.getId());
				
		ops = datastore.createUpdateOperations(Movie.class).set("title", movie.getTitle()).set("movieStatus", movie.getMovieStatus());
		
		datastore.update(updateQuery, ops);
		
	}		

	@Override
	public void remove(Movie movie){
							
		datastore.delete(datastore.createQuery(Movie.class).field("_id").equal(movie.getId()));
				
	}
	
}