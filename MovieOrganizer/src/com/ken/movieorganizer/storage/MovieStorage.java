package com.ken.movieorganizer.storage;

import java.util.List;

import com.ken.movieorganizer.Movie;

/**
 * An interface class for storage of instances of the class Movie.java.
 */
public interface MovieStorage {
	
	/**
	 * Gets a list of all the game titles as Strings.
	 */
	
	public List<String> getList();
	
	/**
	 * Gets all of the games in a list of Movie objects.	 
	 */
	public List<Movie> getAll();
	
	/**
	 * Creates a new Movie on the database.	 
	 */
	public void create(Movie movie);
	
	/**
	 * Updates a Game's Title and Status on the database.
	 */
	public void update(Movie movie);
	
	/**
	 * Deletes a Game from the database
	 */
	public void remove(Movie movie);	


}