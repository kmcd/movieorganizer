package com.ken.movieorganizer.storage.sqlite;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;




import com.google.inject.Inject;
import com.ken.movieorganizer.Movie;
import com.ken.movieorganizer.MovieStatus;
import com.ken.movieorganizer.storage.MovieStorage;

/**
 * 
 * A SQLite specific implementation of MovieStorage.java
 * 
 * Uses a SQLite file found in /MovieOrganizer/sqlschema/yourDbHere
 *
 */

public class SQLiteMovieStorage implements MovieStorage {
	
	Statement statement;
	
	//@Inject	
	public SQLiteMovieStorage(){
		
		Statement statement = null;
		
		try {
			
			 Class.forName("org.sqlite.JDBC");
			
			Connection connection = DriverManager.getConnection("jdbc:sqlite:sqlschema/00initialcreation");
			statement = connection.createStatement();
		    statement.setQueryTimeout(30);
		    
		    this.statement = statement;
		
		} catch (SQLException e) {
			
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
			
		
	}
	
	
	
	@Override
	public List<String> getList() {
		
		List<String> getList = new ArrayList<String>();	
		
		try{
			
			ResultSet rs = statement.executeQuery("SELECT * FROM Movies;");		    
		    
		    while (rs.next()){
		    
			    getList.add(rs.getString("MovieName"));	        
		    
		    }		    	
		
		} 
		
		catch (SQLException e) {
			
			e.printStackTrace();
			
		}
		
		return getList;
		
	}

	@Override
	public List<Movie> getAll() {		
		
		List<Movie> getAllList = new ArrayList<Movie>();	
		
		try{
			
			ResultSet rs = statement.executeQuery("SELECT * FROM Movies ORDER BY MovieName;");		    
		    
		    while (rs.next()){
		    
		    	Movie movie = new Movie();
		    	
		    	MovieStatus movieStatus = MovieStatus.valueOf(rs.getString("MovieStatus"));
		    	
		    	movie.setTitle(rs.getString("MovieName"));
		    	movie.setMovieStatus(movieStatus);
		    	movie.setSQLiteId(rs.getInt("id"));
		    	
		    	getAllList.add(movie);		    	
		    
		    }
		    	
		
		} 
		
		catch (SQLException e) {
			
			e.printStackTrace();
			
		}
		
		return getAllList;	
		
	}

	@Override
	public void create(Movie movie) {
		
		try {
			
			statement.executeUpdate("insert into Movies (MovieName, MovieStatus) values"
					+ "('" + movie.getTitle() + "', '" + movie.getMovieStatus() + "')");
		
		} 
		
		catch (SQLException e) {			
			
			e.printStackTrace();
		}
		
	}

	@Override
	public void update(Movie movie) {
		
		try {
						
			statement.executeUpdate("UPDATE Movies SET MovieName='" + movie.getTitle() + "', "
					+ "MovieStatus ='" + movie.getMovieStatus() + "' WHERE id='" + movie.getSQLiteId() + "';");
					
			
		} catch (SQLException e) {
		
			e.printStackTrace();
			
		}
		
		
	}

	@Override
	public void remove(Movie movie) {

		try {
			
			statement.executeUpdate("DELETE FROM Movies WHERE id='" + movie.getSQLiteId() + "';");
						
		} catch (SQLException e) {			
			
			e.printStackTrace();
		}		    
		
		
	}

}
