package com.ken.movieorganizer;

/**
 * 
 * Contains the Enum for the 4 types of MovieStatuses.
 *
 */
public enum MovieStatus {
	
	HaveSeenMovie, NotSeenMovie, PartialViewing, SeenSpecialFeatures

}
