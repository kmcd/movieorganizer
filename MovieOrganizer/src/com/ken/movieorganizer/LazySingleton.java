package com.ken.movieorganizer;

import javax.swing.SwingUtilities;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



//import com.google.inject.Guice;
//import com.google.inject.Injector;
import com.ken.movieorganizer.gui.view.swing.SwingGUI;
//import com.ken.movieorganizer.guicemodules.SwingSQLiteModule;

/**
 *
 * Creates an Instance of the GUI and ensures only one instance of 
 * the GUI exists at any time. Also initializes all the DI injected
 * Views.
 *
 *context and beans
 */


public class LazySingleton {
    private static volatile LazySingleton instance = null;
    
    

    
    private LazySingleton() {       	
    	
    	@SuppressWarnings("resource")
		final
		ApplicationContext appContext = new ClassPathXmlApplicationContext("file:src/META-INF/spring-context.xml");
       
        final SwingGUI swingGUI = (SwingGUI) appContext.getBean("mainGUI");
        
        //((ConfigurableApplicationContext)appContext).close();
    	
    	/**
    	Injector injView = Guice.createInjector(new SwingSQLiteModule());    	
    	
    	final MainGUI uI = injView.getInstance(MainGUI.class);
    	
    	**/
		
		 SwingUtilities.invokeLater(new Runnable() {
			 
			    public void run() {
			    	
			        swingGUI.createAndShowGUI();
			        
			        
			    }
			    
			});
		
    	   	
    }

    public static LazySingleton getInstance() {
    	
    		
    	
            if (instance == null) {
            	
                    synchronized (LazySingleton .class){
                    	
                            if (instance == null) {
                            	
                            	
                                    instance = new LazySingleton ();
                                    
                            }
                  }
                    
            }
            
            return instance;
            
            
    }
    
}
