package com.ken.movieorganizer;

import org.bson.types.ObjectId;
//import com.google.code.morphia.annotations.Embedded;
//import com.google.code.morphia.annotations.Entity;
//import com.google.code.morphia.annotations.Id;

/**
 * 
 * A model class that contains the components of the
 * Movie. (Title(String), Status(Enum), and Id(ObjectId).
 * These components are accessed via Getter and Setter methods.
 */
//@Entity
public class Movie {
	
		//@Id
		//@Column(name="ID")
	   //@GeneratedValue
		public ObjectId id; 
		
		public int SQLiteId;
	
		//@Embedded
		//@Column(name="Title")
		private String title;		
		
		//@Column(name="MovieStatus")
		private MovieStatus movieStatus;		
		
		public Movie() {
			
			this.title = "New Movie";	
			
		}		
		
		public String getTitle() {
			
			return this.title;
			
		}						
	
		public MovieStatus getMovieStatus(){
			
			return movieStatus;
			
		}		
		
		public  Object getId() {
			
			return id;
						
			
		}
		
		public int getSQLiteId(){
			
			return SQLiteId;
			
		}
		
		public void setTitle(String title) {
			
			this.title = title;
			
		}		
		
		public void setMovieStatus(MovieStatus movieStatus){
			
			this.movieStatus = movieStatus;
			
		}
		
		public void setSQLiteId(int SQLiteId){
			
			this.SQLiteId = SQLiteId;
			
		}
		
		
		
	}