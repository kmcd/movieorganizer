package com.ken.movieorganizer.gui.view;

import com.ken.movieorganizer.gui.presenter.PresenterEditChangeMovie;

/**
 * 
 * An interface program for editing movies from the database. Used in conjunction
 * with EditSelectMovie. EditChangeMovie allows the user to edit the parameters.
 * EditMovie selects the movie the user wishes to edit. 
 * Use presenterEditChange.oKCLicked(String title, String status) when user clicks OK.
 * Use presenterEditObject.cancelClicked() when user clicks Cancel.
 */
public interface EditChangeMovie {
	
	/**
	 * Called by the Presenter, use presenterEditObject.disposeWindow() to access.
	 */
	public void dispose();	
	
	/** 
	 * Called by Presenter, presets text field with old title.
	 */
	public void preSetTextField(String oldTitle); 
	
	/**
	 * Called by Presenter, clears the usable text field.
	 */
	public void clearTextField();
	
	/**
	 * Called by Presenter, shows View
	 * 
	 */
	public void showDialog(PresenterEditChangeMovie presenterEditObject);
	
}