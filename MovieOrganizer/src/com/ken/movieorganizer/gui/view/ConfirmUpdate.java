package com.ken.movieorganizer.gui.view;

import com.ken.movieorganizer.MovieStatus;
import com.ken.movieorganizer.gui.presenter.PresenterMainGUI;

/**
 * 
 * An interface class for a class that brings up a view to 
 * confirm the users choices before proceeding with any changes.
 *
 */
public interface ConfirmUpdate {	
	
	
	public void initialize(PresenterMainGUI presenterMainGUI);
	
	
	/**
	 * 
	 * Used to confirm a choice of adding a movie to the database. Pass in parameters
	 * (String movieName, MovieStatus movieStatus). Returns boolean true of false.	 * 
	 * 
	 */
	public void addMovieConfirm(String movieName, MovieStatus movieStatus);
	
	/**
	 * 
	 * Used to confirm a choice of deleting a movie from the database. Pass in parameters
	 * (String movieName). Returns boolean true of false.	 
	 * 
	 */
	public void delMovieConfirm(String movieName);	
	
	/**
	 * 
	 * 
	 * Used to confirm a choice of editing a movie in the database. Pass in parameters
	 * (String movieTitle, MovieStatus movieStatus, String oldTitle, MovieStatus oldStatus). Returns boolean true of false.	 
	 */
	public void editMovieConfirm(String movieTitle, MovieStatus movieStatus, String oldTitle, MovieStatus oldStatus);
}

	