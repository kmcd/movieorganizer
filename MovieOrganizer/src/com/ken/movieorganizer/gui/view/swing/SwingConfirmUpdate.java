package com.ken.movieorganizer.gui.view.swing;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.springframework.stereotype.Service;

import com.ken.movieorganizer.MovieStatus;
import com.ken.movieorganizer.gui.presenter.PresenterMainGUI;
import com.ken.movieorganizer.gui.view.ConfirmUpdate;

/**
 * 
 * The Swing specific implementation of ConfirmUpdate.java
 *
 */
@SuppressWarnings("serial")

public class SwingConfirmUpdate extends JDialog implements ConfirmUpdate{	
	
	private PresenterMainGUI presenterMainGUI;

	public void initialize(PresenterMainGUI presenterMainGUI){
		
		this.presenterMainGUI = presenterMainGUI;
	}
	
	public void addMovieConfirm(String movieName, MovieStatus movieStatus){	
		
	Boolean okOptionSel = false;
	
	JPanel addMoviePanel = new JPanel();
    
    addMoviePanel.add(new JLabel("Do you wish to Add  '" + movieName + "' of Status '" + movieStatus + "' ?" ));

	int reply = JOptionPane.showConfirmDialog(null, addMoviePanel, "Information Check", JOptionPane.YES_NO_OPTION);
	
	if (reply == JOptionPane.YES_OPTION) {	
	
	okOptionSel = true;	
	
	presenterMainGUI.addConfirmResponse(okOptionSel);
	
	
	}
	
	
	
	}
	
	public void delMovieConfirm(String name){		
		
		Boolean okOptionSel = false;
		
		JPanel addMoviePanel = new JPanel();
	    
	    addMoviePanel.add(new JLabel("Do you wish to Delete  '" + name + "' ?" ));

		int reply = JOptionPane.showConfirmDialog(null, addMoviePanel, "Information Check", JOptionPane.YES_NO_OPTION);		
		
		if (reply == JOptionPane.YES_OPTION) {		
		
		okOptionSel = true;		
		
		}
		
		presenterMainGUI.deleteConfirmResponse(okOptionSel);
		
		}
	
	public void editMovieConfirm(String title, MovieStatus movieStatus, String oldTitle, MovieStatus oldStatus){		
				
		Boolean okOptionSel = false;
		
		JPanel addMoviePanel = new JPanel();
	    
	    addMoviePanel.add(new JLabel("Movie of Title '" + oldTitle + "' of Status '" + oldStatus + "' OK to Edit to Title '" + title + "' of Status '" + movieStatus + "'?" ));

		int reply = JOptionPane.showConfirmDialog(null, addMoviePanel, "Information Check", JOptionPane.YES_NO_OPTION);
		
		if (reply == JOptionPane.YES_OPTION) {		
		
		okOptionSel = true;		
		
		}
		
		presenterMainGUI.editConfirmResponse(okOptionSel);
		
		}

}