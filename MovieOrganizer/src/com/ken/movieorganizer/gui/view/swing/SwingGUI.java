package com.ken.movieorganizer.gui.view.swing;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.DefaultCaret;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.inject.Inject;
import com.ken.movieorganizer.MovieStatus;
import com.ken.movieorganizer.gui.presenter.PresenterMainGUI;
import com.ken.movieorganizer.gui.view.AddMovie;
import com.ken.movieorganizer.gui.view.ConfirmUpdate;
import com.ken.movieorganizer.gui.view.DeleteMovie;
import com.ken.movieorganizer.gui.view.EditSelectMovie;
import com.ken.movieorganizer.gui.view.EditChangeMovie;
import com.ken.movieorganizer.gui.view.MainGUI;
import com.ken.movieorganizer.storage.MovieStorage;

/**
 * 
 * The Swing specific implementation of MainGUI.java
 *
 */
@SuppressWarnings("serial")

public class SwingGUI extends JPanel implements ActionListener, MainGUI {
	    
	   protected JButton addMovie, deleteMovie, editMovie, viewList;
	   protected JTextField textField;
	   protected JTextArea textArea;
	   public int output = 0;
	   public int occured = 0;
	   
	   String status[] = { "Beaten", "Played", "Unplayed" };
	   
	   JFrame frame = new JFrame("MovieOrganizer");       
	   
	   private PresenterMainGUI presenterMainGUI = new PresenterMainGUI(this);
	   	 
	   ConfirmUpdate confirmUpdateMod;
	   
	   MovieStorage movieStorage;	   
	   
	   //@Inject
	   
	   public SwingGUI(final AddMovie addMovieMod, final DeleteMovie deleteMovieMod, final EditSelectMovie editSelectMod, final EditChangeMovie editChangeMod, final ConfirmUpdate confirmUpdateMod, MovieStorage movieStorage) {
		   		   		   
		   super(new GridBagLayout());		   
		  
		   this.confirmUpdateMod = confirmUpdateMod;
		   this.movieStorage = movieStorage;	        

		   textArea = new JTextArea(25, 30);	
	       DefaultCaret caret = (DefaultCaret)textArea.getCaret();
	       caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
	       Font mono = new Font("Monospaced", Font.BOLD, 12);
	       textArea.setFont(mono);
	       textArea.setEditable(false);
	       JScrollPane scrollPane = new JScrollPane(textArea);
	       
	       GridBagConstraints c = new GridBagConstraints();
	       c.gridwidth = GridBagConstraints.REMAINDER;        
	       c.fill = GridBagConstraints.BOTH;
	       c.weightx = 1.0;
	       c.weighty = 1.0;
	       add(scrollPane, c);
	       	       
	       textArea.insert("    Welcome to the MovieOrganizer Graphical User Interface\n",  0);
	       textArea.append("------------------------------------------------------------");
	       	       
	       addMovie = new JButton("Add a Movie");
		   addMovie.addActionListener(
				   new ActionListener() {
		    		    public void actionPerformed(ActionEvent e) {		    		    	
		    		    			    		    
		    		    	presenterMainGUI.showAddMovieDialog(addMovieMod);	    		    	 		    	
		    		        
		    		      }
				   		}		    		    
		    		  
				   );
		   
	        deleteMovie = new JButton("Delete a Movie");
	        deleteMovie.addActionListener(
		    		  new ActionListener() {
		    		    public void actionPerformed(ActionEvent e) {
		    		    	
		    		    	presenterMainGUI.showDeleteMovieDialog(deleteMovieMod);   	
		    		    	
		    		    }
		    		    
		    		  }  	
		    		    	
	        		);
	        
	        editMovie = new JButton("Edit a Movie");
	        editMovie.addActionListener(
		    		  new ActionListener() {
		    		    public void actionPerformed(ActionEvent e) {	    		    	
		    		    	
		    		    	presenterMainGUI.showEditMovieDialog(editSelectMod, editChangeMod);		    		    	
		    		    			    		    	 
		    		    }
		    		  }
		    		  
		    		);
	        
	        viewList = new JButton("View Movie List");
	        viewList.addActionListener(
		    		  new ActionListener() {
		    		    public void actionPerformed(ActionEvent e) {
		    		    	
		    		    	textArea.append("\n");
		    		    	presenterMainGUI.presViewMovieList();		    		    	
		    		    	
		    		    }
		    		    
		    		  }
		    		  
		    		);  
	        
	        add(addMovie);
	        add(deleteMovie);
	        add(editMovie);
	        add(viewList);   
	        		  
	   }	   
	  
	   @Override
	   public void createAndShowGUI()  {		
	       
	        presenterMainGUI.setConfirmUpdate(confirmUpdateMod);
	        presenterMainGUI.setMovieStorage(movieStorage); 
	       	         
	        frame.setContentPane(this);	     
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  	       
	        frame.pack();
	        frame.setLocationRelativeTo(null);
	        frame.setVisible(true);    
	       
	    }  
	  
	   @Override
	   public void successAdd(String movieTitle, MovieStatus movieStatus){
		   
		   textArea.append("\n" + "Movie to be Added" + "\n" + "Title '" + movieTitle + "'\nStatus '" + movieStatus + "'\nSuccessfully added.\n");
		   
	   }
	   
	   @Override
	   public void successDelete(String movieTitle, MovieStatus movieStatus){
		   
		   textArea.append("\n" + "Movie to be Deleted" + "\n" + "Title '" + movieTitle + "'\nStatus '" + movieStatus + "'\nSuccessfully deleted.\n");
		   
	   }   
	  
	   @Override
	   public void successEdit(String title, MovieStatus movieStatus, String oldTitle, MovieStatus oldStatus){
	   
		   textArea.append("\nMovie Title '" + oldTitle + "'\nOf Status '" + oldStatus + "' \nEditted  to Title '" + title + "'\nOf Status '" + movieStatus + "'\n");
	   
	   }
	   
	   @Override
	   public void appendMovies(String movieViewList){
		   		   
		   textArea.append(movieViewList);
		   
	   }
	   
	   @Override
	   public void noMovies(){
		
		   textArea.append("No Movies Found \n");
		   
	   }
	   
	   @Override
	   public void moviePreexists(String movieTitle){
		   
		   textArea.append("\n" + "Movie of Title '" + movieTitle + "'\nalready exists.\n");
		   
	   }
	   
	   @Override
	   public void movieAndStatusPreexists(String movieTitle, MovieStatus movieStatus){
		   
		   textArea.append("\n" + "Movie of Title '" + movieTitle + "'\nand Status '" + movieStatus + "' already exists.\n");
		   
	   }
	   
	   @Override
	   public void actionPerformed(ActionEvent e) {			
			
	   }

	@Override
	public void addGame(String name, String status) {
		// Not Used		
	}

	@Override
	public void setController(Object controller) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void confirmAdd(String movieName, MovieStatus movieStatus) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addGameConfirmed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void viewGameListJSON() {
		// TODO Auto-generated method stub
		
	}
		
		
}