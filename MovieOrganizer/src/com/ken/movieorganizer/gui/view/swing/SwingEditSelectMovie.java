package com.ken.movieorganizer.gui.view.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.springframework.stereotype.Service;

import com.ken.movieorganizer.gui.presenter.PresenterEditSelectMovie;
import com.ken.movieorganizer.gui.view.EditSelectMovie;

/**
 * 
 * The Swing specific implementation of EditSelectMovie.java
 *
 */
@SuppressWarnings("serial")

public class SwingEditSelectMovie extends JDialog implements ActionListener, ListSelectionListener, EditSelectMovie {
	
	String[] status = { "Beaten", "Played", "Unplayed" };
	protected JButton ok, cancel;		
	private PresenterEditSelectMovie presenterEditMovie;	
	JList<String> jList = new JList<String>();	
	final JPanel buttonPane = new JPanel();
	final JPanel messagePane = new JPanel();
	JScrollPane listScrollPane = new JScrollPane();	
	
	public SwingEditSelectMovie(){		
		
		JDialog JDialogIO = this;		
		JDialogIO.setTitle("Please Select the Movie you wish to Delete");
		JDialogIO.setResizable(false);
		JDialogIO.setModal(true);	
	    
	    final JButton okButton = new JButton("OK");	    
	    okButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {   
		         
		    presenterEditMovie.oKClicked((int) jList.getSelectedIndex());	  
		    	    		    
		    }
		    	
		    }
	    
	   );
	    
	    final JButton cancelButton = new JButton("Cancel");
	    cancelButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    
		    presenterEditMovie.cancelClicked();
		    
		    }
		    	
		    }
	    
	   );    	    
	           
        messagePane.add(new JLabel("Name of Movie:"));
        messagePane.setPreferredSize(new Dimension(450,50));
        buttonPane.add(okButton);
        buttonPane.add(cancelButton);    
	        	  
	    }

	@Override
	public void dispose(){
		
		super.dispose();
		
	}	
		
	public void setListView(String[] movieTitleList, int movieAmount){
		
		final JList<String> jList = new JList<String>(movieTitleList);
		
        jList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jList.setSelectedIndex(0);
        jList.addListSelectionListener(this);       
        jList.setVisibleRowCount(movieAmount);
        	
        final JScrollPane listScrollPane = new JScrollPane(jList);        
       
        this.jList = jList;
        this.listScrollPane = listScrollPane;            
		
	}
	
	public void clearWindow(){
		
		getContentPane().removeAll();
		
	}	
	
	public void showDialog(PresenterEditSelectMovie presenterEditMovie) {
				
		this.presenterEditMovie = presenterEditMovie;		
		
		getContentPane().setPreferredSize(new Dimension(350, 350));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		getContentPane().add(messagePane, BorderLayout.NORTH);		
		getContentPane().add(listScrollPane, BorderLayout.CENTER);	
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	@Override
	public void valueChanged(ListSelectionEvent e) {
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
				
	}
	
}	              