package com.ken.movieorganizer.gui.view.swing;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.Document;

import org.springframework.stereotype.Service;

import com.ken.movieorganizer.MovieStatus;
import com.ken.movieorganizer.gui.presenter.PresenterEditChangeMovie;
import com.ken.movieorganizer.gui.view.EditChangeMovie;

/**
 * 
 * The Swing specific implementation of EditChangeMovie.java
 *
 */
@SuppressWarnings("serial")

public class SwingEditChangeMovie extends JDialog implements ActionListener,
		ListSelectionListener, EditChangeMovie {

	protected JButton ok, cancel;
	private DefaultListModel<String> listModel;
	protected JList<String> list;
	final String[] movieTitleAndStatus = new String[] { "MovieName", null };
	final JPanel messagePane = new JPanel();
	final JPanel buttonPane = new JPanel();	
	final JTextField movieNameField = new JTextField(20);
	private String oldTitle;	

	private PresenterEditChangeMovie presenterEditObject;
	
	public SwingEditChangeMovie() {

		setTitle("Please Enter Movie Name and Select Status");
		setResizable(false);
		setModal(true);

		final JButton okButton = new JButton("OK");
		okButton.setEnabled(false);
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				presenterEditObject.oKClicked(movieNameField.getText(), list.getSelectedValue());

			}

		});
		
		final JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				presenterEditObject.cancelClicked();
			}

		});

		movieNameField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent e) {

				Document document = e.getDocument();
				int docLength = document.getLength();
				okButton.setEnabled(presenterEditObject
						.titleLengthCheck(docLength));

			}

			@Override
			public void insertUpdate(DocumentEvent e) {

				Document document = e.getDocument();
				int docLength = document.getLength();
				okButton.setEnabled(presenterEditObject
						.titleLengthCheck(docLength));

			}

			@Override
			public void removeUpdate(DocumentEvent e) {

				Document document = e.getDocument();
				int docLength = document.getLength();
				okButton.setEnabled(presenterEditObject
						.titleLengthCheck(docLength));

			}

		});

		listModel = new DefaultListModel<String>();
		listModel.addElement(MovieStatus.HaveSeenMovie.toString());
		listModel.addElement(MovieStatus.NotSeenMovie.toString());
		listModel.addElement(MovieStatus.PartialViewing.toString());
		listModel.addElement(MovieStatus.SeenSpecialFeatures.toString());
		list = new JList<String>(listModel);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setSelectedIndex(0);
		list.addListSelectionListener(this);
		list.setVisibleRowCount(4);
		 DefaultListCellRenderer renderer =  
                 (DefaultListCellRenderer)list.getCellRenderer();  
		 renderer.setHorizontalAlignment(JLabel.CENTER);  

		messagePane.add(new JLabel("Name of Movie:"));
		messagePane.add(movieNameField);
		messagePane.add(new JLabel("Status of Movie:"));
		buttonPane.add(okButton);
		buttonPane.add(cancelButton);

	}

	@Override
	public void dispose() {

		super.dispose();
	}
	
	public void preSetTextField(String oldTitle){
		
		this.oldTitle = oldTitle;
		
	}
	
	public void clearTextField(){
		
		movieNameField.setText("");
	}	

	public void showDialog(PresenterEditChangeMovie presenterEditObject) {

		this.presenterEditObject = presenterEditObject;
		movieNameField.setText(oldTitle);
		
		getContentPane().add(list, BorderLayout.EAST);
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		getContentPane().add(messagePane, BorderLayout.WEST);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);

	}

	@Override
	public void valueChanged(ListSelectionEvent arg0) {
	
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
	}

}