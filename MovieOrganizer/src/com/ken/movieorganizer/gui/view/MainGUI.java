package com.ken.movieorganizer.gui.view;

import com.ken.movieorganizer.MovieStatus;


/**
 * 
 * The interface for the Main Screen used in the MovieOrganizer Program.
 *
 * use PresenterMainGUI.setConfirmUpdate(confirmUpdate)
 * to pass the instance of confirmUpdate from the constructor.
 * 
 * us PresenterMainGui.setMovieStorage(movieStorage);
 * to pass the instance of movieStorage from the constructor.
 */
public interface MainGUI {
	
	/**
	 * Starts the GUI.	
	 */
	public void createAndShowGUI();	
	
	/**
	 * //display message that the movie with MovieTitle movieTitle and 
	 * MovieStatus movieStatus was successfully added.
	 * 
	 */
	public void successAdd(String movieTitle, MovieStatus movieStatus); 
	
	/**
	 * Display a message that no movies were found.
	 */
	public void noMovies(); 
	
	/**
	 * Display a message that the movie of prior Title oldTitle and prior Status oldStatus 
	 * was successfully changed to Title title and Status status.
	 */
	public void successEdit(String title, MovieStatus movieStatus, String oldTitle, MovieStatus oldStatus);//display
	
	/**
	 * Appends the whole movie list with each movie's title and status. Formated as such
	 * String.format("%-40s%20s", title, status + "\r\n")
	 */
	public void appendMovies(String movieViewList);
	
	/**
	 * Display message that movie the movie with title movieTitle and status 
	 * movieStatus was deleted.
	 */
	public void successDelete(String movieTitle, MovieStatus movieStatus); 
	
	/**
	 * Display the movie of Title movieTitle already exists.
	 */
	public void moviePreexists(String movieTitle); 
	
	/**
	 * Display the movie of title movieTitle and Status movieStatus already exists.
	 */
	public void movieAndStatusPreexists(String movieTitle, MovieStatus oldStatus);

	public void addGame(String name, String status);

	public void setController(Object controller);

	public void confirmAdd(String movieName, MovieStatus movieStatus);

	public void addGameConfirmed();
	
	public void viewGameListJSON();	
		
	

}