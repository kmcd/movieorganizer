package com.ken.movieorganizer.gui.view;

import com.ken.movieorganizer.gui.presenter.PresenterDeleteMovie;

/**
 * 
 * An interface class for deleting movies from the database. 
 * Use presenterDeleteMovie.oKCLicked(int selectedMovieIndexNumber) when user clicks OK.
 * Use presenterDeleteMovie.cancelClicked() when user clicks Cancel.
 */
public interface DeleteMovie {
	
	/**
	 * Called by the Presenter, use presenterDeleteMovie.disposeWindow() to access.
	 */
	public void dispose(); 
	
	/**
	 * Called by the Presenter, clear everything from the View.
	 */
	public void clearWindow(); 
	
	/**
	 * 
	 * Called by the Presenter, sets up the MovieList. 
	 * String array contains all of the movies from the database, movieAmount is 
	 * the # of movies in the Database.
	 * 
	 */
	public void setListView(String[] arrayNames , int movieAmount);
	
	/**
	 * Called by Presenter, shows View.
	 */
	public void showDialog(PresenterDeleteMovie presenterDeleteMOvie);
	
}