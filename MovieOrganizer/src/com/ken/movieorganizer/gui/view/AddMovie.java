package com.ken.movieorganizer.gui.view;

import com.ken.movieorganizer.gui.presenter.PresenterAddMovie;

/**
 * An interface class for adding movies to the database. 
 * Use presenterAddMovie.oKCLicked(String title, String status) when user clicks OK.
 * Use presenterAddMovie.cancelClicked() when user clicks Cancel.
 * Use presenterAddMovie.titleLengthCheck() to ensure the usable text field
 * contains a string, returns true if String Length > 0	.
 */
public interface AddMovie {
	
	/**
	 * Called by the Presenter, use presenterAddMovie.disposeWindow()to access.
	 */
	public void dispose();
	
	/**
	 * Called by Presenter, clears the usable text field.
	 */
	public void clearTextField();
	
	/**
	 * Called by Presenter, shows View.
	*/
	public void showDialog(PresenterAddMovie presenterAddMovie);

	public void setNameAndStatus(String name, String status); 
	
	}