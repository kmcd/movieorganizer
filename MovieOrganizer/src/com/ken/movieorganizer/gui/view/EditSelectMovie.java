package com.ken.movieorganizer.gui.view;

import com.ken.movieorganizer.gui.presenter.PresenterEditSelectMovie;

/**
 * An interface program for editing movies from the database. Used in conjunction
 * with EditObject. EditMovie selects the movie the user wishes to edit. Edit Object
 * allows the user to edit the parameters. 
 * Use presenterDeleteMovie.oKCLicked(int selectedMovieIndexNumber) when user clicks OK.
 * Use presenterDeleteMovie.cancelClicked() when user clicks Cancel.	
 */
public interface EditSelectMovie {
	
		/**
		 * Called by the Presenter, use presenterEditMovie.disposeWindow() to access.
		 */
		public void dispose(); 
		
		/**
		 * Called by the Presenter, clear everything from the View.
		 */
		public void clearWindow(); 
		
		/**
		 * Called by the Presenter, set up the MovieList. String[] arrayNames contains 
		 * all of the movies from the database, movieAmount is the # of movies in the 
		 * Database.
		 *
		 */
		public void setListView(String[] arrayNames , int movieAmount);// 
		
		/**
		 * Called by the Presenter, shows View. 
		 */
		public void showDialog(PresenterEditSelectMovie presenterEditMovie);
	
	
}