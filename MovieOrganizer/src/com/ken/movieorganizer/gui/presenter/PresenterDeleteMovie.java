package com.ken.movieorganizer.gui.presenter;

import java.util.ArrayList;
import java.util.List;
import com.ken.movieorganizer.Movie;
import com.ken.movieorganizer.gui.view.DeleteMovie;
import com.ken.movieorganizer.storage.MovieStorage;

public class PresenterDeleteMovie {	
		
	PresenterMainGUI presenterMainGUI;
	
	DeleteMovie deleteMovieMod;
	
	MovieStorage movieStorage;	
	
	public String[] arrayNames;
	
	private int movieAmount;
		
	public PresenterDeleteMovie(PresenterMainGUI presenterMainGUI){
		
		this.presenterMainGUI = presenterMainGUI;
		
	}
	
	public void create(DeleteMovie deleteMovieMod, MovieStorage movieStorage){
		
		this.movieStorage = movieStorage;
		
		List<Movie> listMovie = movieStorage.getAll();
		
		this.deleteMovieMod = deleteMovieMod;
		
		ArrayList<String> movieTitleList = new ArrayList<String>();
		
		for (Movie movie : listMovie){			
			
			movieTitleList.add(movie.getTitle());			
			
		}				
		
		java.util.Arrays.sort(arrayNames = movieTitleList.toArray(new String[movieTitleList.size()]));
		
		movieAmount = arrayNames.length;		
		
		if (movieAmount > 0){		
		
			deleteMovieMod.clearWindow();		
			deleteMovieMod.setListView(arrayNames ,movieAmount);		
			deleteMovieMod.showDialog(this);
			
		}
		
		if (movieAmount == 0){
			
			this.presenterMainGUI.presNoMovies();
			
		}
		
	}
	
	public void oKClicked(int movieIndex){		
		
		List<Movie> listMovie = movieStorage.getAll();
		
		this.deleteMovieMod.dispose();	
				
		this.presenterMainGUI.presMovieDelete(listMovie.get(movieIndex));	
			
	}
	
	public void cancelClicked(){
		
		this.deleteMovieMod.dispose();
		
	}
	
	
	public boolean titleLengthCheck(int titleLength) {
				
		if (titleLength > 0) {
			
			return true;
			
		} else {
			
			return false;
			
		}

	}

}