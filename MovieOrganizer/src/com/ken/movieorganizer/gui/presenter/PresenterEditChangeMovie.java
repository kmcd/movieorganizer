package com.ken.movieorganizer.gui.presenter;

import java.util.List;
import com.ken.movieorganizer.Movie;
import com.ken.movieorganizer.MovieStatus;
import com.ken.movieorganizer.gui.view.EditChangeMovie;
import com.ken.movieorganizer.storage.MovieStorage;

public class PresenterEditChangeMovie {
	
	private PresenterEditSelectMovie presenterEditSelectMovie;
	
	private Movie movie;
	
	MovieStorage movieStorage;
	
	EditChangeMovie editObjectMod;	
	
	public PresenterEditChangeMovie (PresenterEditSelectMovie presenterEditSelectMovie){
		
		this.presenterEditSelectMovie = presenterEditSelectMovie;
		
	}
	
	public void create(Movie inputMovie, EditChangeMovie editObjectMod, MovieStorage movieStorage){
		
		this.editObjectMod = editObjectMod;
		
		this.movie = inputMovie;
		
		this.movieStorage = movieStorage;
		
		editObjectMod.preSetTextField(inputMovie.getTitle());
				
		editObjectMod.showDialog(this);		
		
	}	
	
	public void oKClicked(String enteredTitle, String stringMovieStatus){
		
		String oldTitle = movie.getTitle();
		MovieStatus oldStatus = movie.getMovieStatus();
		
		MovieStatus movieStatus = MovieStatus.valueOf(stringMovieStatus);
		
		List<String> names = movieStorage.getList();		
		
		Boolean alreadyInList = false;
		Boolean sameStatus =false;
				
		for (String movieInList: names){
			
			if (alreadyInList == false && enteredTitle.equals(movieInList) == true){
							
				alreadyInList = true;				
				
			}
		}		
		
		if (oldStatus.equals(movieStatus) == true ){
			
			sameStatus = true;
			
			
		}
		
		if (alreadyInList == true && sameStatus == true){
			
			this.presenterEditSelectMovie.movieAndStatusPreexists(oldTitle, oldStatus);
			editObjectMod.dispose();
			
		}
		
		else if (alreadyInList == false || sameStatus == false){
			
			movie.setMovieStatus(movieStatus);		
			movie.setTitle(enteredTitle);	
			editObjectMod.dispose();
			this.presenterEditSelectMovie.movieEdit(movie, oldTitle, oldStatus);		
			
		}	
		
	}	
	
	public void cancelClicked(){
		
		editObjectMod.dispose();				
		
	}	

	public boolean titleLengthCheck(int titleLength) {				

		if (titleLength > 0) {
			return true;
		} else {
			return false;
		}

	}

}