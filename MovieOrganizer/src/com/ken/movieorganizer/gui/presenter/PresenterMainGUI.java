package com.ken.movieorganizer.gui.presenter;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.ken.movieorganizer.Movie;
import com.ken.movieorganizer.MovieStatus;
import com.ken.movieorganizer.gui.view.AddMovie;
import com.ken.movieorganizer.gui.view.ConfirmUpdate;
import com.ken.movieorganizer.gui.view.DeleteMovie;
import com.ken.movieorganizer.gui.view.EditSelectMovie;
import com.ken.movieorganizer.gui.view.EditChangeMovie;
import com.ken.movieorganizer.gui.view.MainGUI;
import com.ken.movieorganizer.storage.MovieStorage;

public class PresenterMainGUI {
	
	private MainGUI mainGUI;
	PresenterAddMovie presenterAddMovie = new PresenterAddMovie(this);
	PresenterDeleteMovie presenterDeleteMovie = new PresenterDeleteMovie(this);
	PresenterEditSelectMovie presenterEditMovie = new PresenterEditSelectMovie(this);
	
	ConfirmUpdate confirmUpdate;
	MovieStorage movieStorage;
	Boolean confirmResponse;	
	String movieTitle;
	MovieStatus movieStatus;
	Movie movieToDelete;
	Movie movieToEdit;
	String oldTitle;
	MovieStatus oldStatus;
	
	public PresenterMainGUI(MainGUI mainGUI){
		
		this.mainGUI = mainGUI;
		
		
	}
		
	public void setConfirmUpdate(ConfirmUpdate confirmUpdateMod) {
		
		confirmUpdateMod.initialize(this);
		this.confirmUpdate = confirmUpdateMod;
		
		
	}
	

	
	public void setMovieStorage(MovieStorage movieStorage) {
		
		this.movieStorage = movieStorage;
		
	}
	
	public void showAddMovieDialog(AddMovie addMovieMod){		
		
		presenterAddMovie.create(addMovieMod, movieStorage);
		
	}
	
	public void showDeleteMovieDialog(DeleteMovie deleteMovieMod){
					
		presenterDeleteMovie.create(deleteMovieMod, movieStorage);
		
	}
	
	public void showEditMovieDialog(EditSelectMovie editMovieMod, EditChangeMovie editMovieObject){
				
		presenterEditMovie.create(editMovieMod, editMovieObject, movieStorage);
		
	}
	
	public void presNameAndStatAdd(String movieTitle, MovieStatus movieStatus){		
			
		this.movieStatus = movieStatus;
		this.movieTitle = movieTitle;
		
		confirmUpdate.addMovieConfirm(movieTitle, movieStatus);	
	}
	
	public void addConfirmResponse(Boolean confirmResponse){	
		
		if (confirmResponse == true){			
			
			Movie myMovie = new Movie();
			
			myMovie.setTitle(movieTitle);
		  	  
		  	myMovie.setMovieStatus(movieStatus);  	    
	  	    	  
		  	movieStorage.create(myMovie);	  	
		  	
		  	this.mainGUI.successAdd(movieTitle, movieStatus);	 
		  				
			}
		
	}		
		

	
	public void presMovieDelete(Movie movieToDelete){		
		
		this.movieToDelete = movieToDelete;
		confirmUpdate.delMovieConfirm(movieToDelete.getTitle());
		
		 
		   
	}	
	
	public void deleteConfirmResponse(Boolean confirmResponse) {
		
		if (confirmResponse == true){			
			
			movieStorage.remove(movieToDelete);
			
			this.mainGUI.successDelete(movieToDelete.getTitle(), movieToDelete.getMovieStatus());
			
		}		
		
	}
	
	public void presMovieEdit(Movie movieToEdit, String oldTitle, MovieStatus oldStatus){		
				
		if (movieToEdit != null && oldTitle != null){
		
			this.oldStatus = oldStatus;
			this.oldTitle = oldTitle;
			this.movieToEdit = movieToEdit;
			
			confirmUpdate.editMovieConfirm(movieToEdit.getTitle(), movieToEdit.getMovieStatus(), oldTitle, oldStatus);
						
			}
				
		}   
	
	public void editConfirmResponse(Boolean confirmResponse){
		
		
		
		if (confirmResponse == true){				
			
			movieStorage.update(movieToEdit);
			
			this.mainGUI.successEdit(movieToEdit.getTitle(), movieToEdit.getMovieStatus(), oldTitle, oldStatus);			
					
		}
		   
	}
	


	
	
	public void presViewMovieList(){		
   	 
    	List<String> stringMovieList = new ArrayList<String>();  	
    	   	
    	List<Movie> getAllList = movieStorage.getAll();
    	
    	String formattedOutput = "Movie title and Status";    	
    	
    	for (Movie outputMovies: getAllList){
    		
    		formattedOutput = String.format("%-40s%20s", outputMovies.getTitle(), outputMovies.getMovieStatus() + "\r\n");
    		
    		stringMovieList.add(formattedOutput);    		
    		
    	}
    	
    	if (stringMovieList.isEmpty() == false){
    		
    		for (String dispMovie : stringMovieList){
    			
    			this.mainGUI.appendMovies(dispMovie);
    		
    		}
    	}
    	
    	else {
    		
    		this.mainGUI.noMovies();
    		
    	}
		
	}
	
	public void getMovieListJSON(){		
	   	 
    			
    	   	
    	List<Movie> getAllList = movieStorage.getAll();
    	
    	if (getAllList.isEmpty() != true){
    		    		
        	
        	JSONMovieView jsonMovieView = new JSONMovieView(getAllList);    	
        	
        	Gson gson = new Gson();        	
        	  	
        	String movieList = gson.toJson(jsonMovieView); 	
    		
        	mainGUI.appendMovies(movieList);
    		
    	}
    	
    	else {
    		
    		this.mainGUI.noMovies();
    	
    		
    	}
		
	}
	
	
	public void presNoMovies(){
		
		this.mainGUI.noMovies();
		
	}
	
	public void presMoviePreexists(String title){
		
		this.mainGUI.moviePreexists(title);
		
	}
	
	public void presMovieAndStatusPreexists(String title, MovieStatus oldStatus){
		
		this.mainGUI.movieAndStatusPreexists(title, oldStatus);
		
	}

	public void confirmAdd(String movieName, MovieStatus movieStatus) {
		

		mainGUI.confirmAdd(movieName, movieStatus);
		
	}

	




	
	
}