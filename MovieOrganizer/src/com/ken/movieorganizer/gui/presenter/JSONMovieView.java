package com.ken.movieorganizer.gui.presenter;

import com.ken.movieorganizer.Movie;
import java.util.ArrayList;
import java.util.List;

public class JSONMovieView {
	
	public JSONMovieView(List<Movie> movieList){
		
		
		List<String> status = new ArrayList<String>();
		List<String> title = new ArrayList<String>();
		
		for (Movie movie : movieList){
			
			
			
			title.add(movie.getTitle());
			status.add(movie.getMovieStatus().toString());
						
			
			
		}
		
		this.title = title;
		this.status = status;
		
		
	}
	
	List<String> title;
	List<String> status;

}
