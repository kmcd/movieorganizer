package com.ken.movieorganizer.gui.presenter;

import java.util.List;
import com.ken.movieorganizer.MovieStatus;
import com.ken.movieorganizer.gui.view.AddMovie;
import com.ken.movieorganizer.storage.MovieStorage;

public class PresenterAddMovie {	
	
	PresenterMainGUI presenterMainGUI;
	AddMovie addMovieMod;
	MovieStorage movieStorage;
	
	public PresenterAddMovie(PresenterMainGUI presenterMainGUI){
		
		this.presenterMainGUI = presenterMainGUI;
		
	}
	
	public void create(AddMovie addMovieMod, MovieStorage movieStorage){
		
		this.addMovieMod = addMovieMod;
		this.movieStorage = movieStorage;
		addMovieMod.showDialog(this);
		
	}
	
	public void oKClicked(String movieTitle, String stringMovieStatus){
				
		MovieStatus movieStatus = MovieStatus.valueOf(stringMovieStatus);
				
		List<String> names = movieStorage.getList();	
		
		Boolean alreadyInList = false;		
		
		if (names != null){
		
			for (String movieInList: names){
				
				if (alreadyInList == false && movieTitle.equals(movieInList) == true){
								
					alreadyInList = true;
										
				}
			}	
		}
		
		if (alreadyInList == true){
			
			this.presenterMainGUI.presMoviePreexists(movieTitle);
			addMovieMod.dispose();
			addMovieMod.clearTextField();
			
		}
		
		else{
		
			addMovieMod.dispose();
			addMovieMod.clearTextField();
			
			this.presenterMainGUI.presNameAndStatAdd(movieTitle, movieStatus);
			
		}
		
	}
	
	public void cancelClicked(){
		
		addMovieMod.dispose();
		
	}
	
	public boolean titleLengthCheck(int titleLength) {				

		if (titleLength > 0) {
			
			return true;
			
		} else {
			
			return false;
			
		}

	}

}