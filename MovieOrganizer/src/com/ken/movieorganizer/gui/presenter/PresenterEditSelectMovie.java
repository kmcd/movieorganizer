package com.ken.movieorganizer.gui.presenter;

import java.util.ArrayList;
import java.util.List;
import com.ken.movieorganizer.Movie;
import com.ken.movieorganizer.MovieStatus;
import com.ken.movieorganizer.gui.view.EditSelectMovie;
import com.ken.movieorganizer.gui.view.EditChangeMovie;
import com.ken.movieorganizer.storage.MovieStorage;

public class PresenterEditSelectMovie {
	
	PresenterEditChangeMovie presenterEditChangeMovie = new PresenterEditChangeMovie(this);
		
	PresenterMainGUI presenterMainGUI;
	
	private EditSelectMovie editMovieMod;
	
	private EditChangeMovie editObjectMod;
	
	MovieStorage movieStorage;
		
	public String[] arrayNames;
	
	private int movieAmount;
		
	public PresenterEditSelectMovie(PresenterMainGUI presenterMainGUI){
		
		this.presenterMainGUI = presenterMainGUI;
		
	}	

	public void create(EditSelectMovie editMovieMod, EditChangeMovie editObjectMod, MovieStorage movieStorage){
		
		List<Movie> listMovie = movieStorage.getAll();
		
		this.editMovieMod = editMovieMod;
		this.editObjectMod = editObjectMod;
		this.movieStorage = movieStorage;
		
		ArrayList<String> movieTitleList = new ArrayList<String>();
		
		for (Movie movie : listMovie){			
			
			movieTitleList.add(movie.getTitle());				
			
		}				
		
		java.util.Arrays.sort(arrayNames = movieTitleList.toArray(new String[movieTitleList.size()]));
		
		movieAmount = arrayNames.length;		
		
		if (movieAmount == 0){			
			
			this.presenterMainGUI.presNoMovies();			
			
		}		
		
		if (movieAmount > 0){		
		
			editMovieMod.clearWindow();		
			editMovieMod.setListView(arrayNames ,movieAmount);		
			editMovieMod.showDialog(this);
			
		}	
		
	}
	
	public void oKClicked(int movieIndex){		
		
		List<Movie> listMovie = movieStorage.getAll();
		
		editMovieMod.dispose();	
				
		this.presenterEditChangeMovie.create(listMovie.get(movieIndex), editObjectMod, movieStorage);				
			
	}
	
	public void movieAndStatusPreexists(String oldTitle, MovieStatus oldStatus){
		
		this.presenterMainGUI.presMovieAndStatusPreexists(oldTitle, oldStatus);
		
	}
	
	public void movieEdit(Movie movie, String oldTitle, MovieStatus oldStatus) {


		this.presenterMainGUI.presMovieEdit(movie, oldTitle, oldStatus);
		
	}	
	
	public void cancelClicked(){
		editMovieMod.dispose();
	}		

	public boolean titleLengthCheck(int titleLength) {				

		if (titleLength > 0) {
			
			return true;
			
		} else {
			
			return false;
			
		}

	}	

}