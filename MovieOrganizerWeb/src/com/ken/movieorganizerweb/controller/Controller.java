package com.ken.movieorganizerweb.controller;




import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.ken.movieorganizer.MovieStatus;
import com.ken.movieorganizer.gui.view.MainGUI;
import com.ken.movieorganizerweb.MainServlet;
import com.ken.movieorganizerweb.module.WebSQLiteModule;

public class Controller {
	
	MainServlet mainServlet;
	
	
	public Controller(MainServlet mainServlet){
		
		this.mainServlet = mainServlet;
		
		uI.setController(this);
		
		
	}
	
	
	Injector injView = Guice.createInjector(new WebSQLiteModule());    	
	
	final MainGUI uI = injView.getInstance(MainGUI.class);



	public void addGame(String name, String status) {
	
	uI.addGame(name, status);
    
    }
	
	public void moviePreExistsCont(String movieTitle){
		
		mainServlet.moviePreExists(movieTitle);
		
		
	}

	public void confirmAdd(String movieName, MovieStatus movieStatus) {


		mainServlet.confirmAdd(movieName, movieStatus);
		
	}

	public void addGameConfirmed() {
		
		uI.addGameConfirmed();
		
		
	}

	public void successAdd(String movieTitle, MovieStatus movieStatus) {

		mainServlet.successAdd(movieTitle, movieStatus);
		
		
	}
	
	public void getMovieList(){
		
		uI.viewGameListJSON();
		
	}
	
	public void appendMovieList(String movieList){
		
		
		
	}
	
	
	
			
		
}			
	
		
		 
	

