package com.ken.movieorganizerweb.jsonobject;

public class JSONObject {
	
	public JSONObject(String identifier, String name, String status, String oldName, String oldStatus){
		
		this.identifier = identifier;
		this.name = name;
		this.status = status;
		this.oldName = oldName;
		this.oldStatus = oldStatus;
		
		
	}
	
	String identifier;
	String name;
	String status;
	String oldName;
	String oldStatus;

}
