package com.ken.movieorganizerweb;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.ken.movieorganizer.MovieStatus;
import com.ken.movieorganizerweb.controller.Controller;
import com.ken.movieorganizerweb.jsonobject.JSONObject;











@WebServlet("/MainServlet")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Controller controller = new Controller(this);
	String title;
	String status;
	HttpServletRequest request;
	HttpServletResponse response;
	
    public MainServlet() {
        super();      
    }
    
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

				
				this.request = request;
				this.response = response;
		
			if (request.getParameter("type").equals("addGameStart")){
		
				title = request.getParameter("name");
		        status = request.getParameter("status");     		        
		        controller.addGame(title, status);	   	        
		            
		        
			}
			
			if (request.getParameter("type").equals("addGameConfirmed")){
				     		        
		        controller.addGameConfirmed();	   	        
		            
		        
			}
			
			if (request.getParameter("type").equals("viewMovieList")){
 		        
		        controller.getMovieList();    
		        
			}
			
			
		
	}
	
	public void moviePreExists(String movieTitle){	
		
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		JSONObject object = new JSONObject ("Preexists", movieTitle, null, null, null);		
		
		Gson gson = new Gson();		
		
		try {
			response.getWriter().write(gson.toJson(object));
		} catch (IOException e) {
	
			e.printStackTrace();
		}
		
		
	}


	public void confirmAdd(String movieTitle, MovieStatus movieStatus) {

		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		
		String movieStatusString = movieStatus.toString();
		
		JSONObject object = new JSONObject ("OK", movieTitle, movieStatusString, null, null);		
		
		Gson gson = new Gson();		
		
		try {
			response.getWriter().write(gson.toJson(object));
		} catch (IOException e) {
	
			e.printStackTrace();
		}
		
	}


	public void successAdd(String movieTitle, MovieStatus movieStatus) {


		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		
		String movieStatusString = movieStatus.toString();
		
		JSONObject object = new JSONObject ("Success", movieTitle, movieStatusString, null, null);		
		
		Gson gson = new Gson();		
		
		try {
			response.getWriter().write(gson.toJson(object));
		} catch (IOException e) {
	
			e.printStackTrace();
		}
		
		
	}


}
