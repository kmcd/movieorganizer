package com.ken.movieorganizerweb.view;

import com.ken.movieorganizer.MovieStatus;
import com.ken.movieorganizer.gui.presenter.PresenterMainGUI;
import com.ken.movieorganizer.gui.view.ConfirmUpdate;



public class WebConfirmUpdate implements ConfirmUpdate{
	
	PresenterMainGUI presenterMainGUI;
	
	@Override
	public void initialize(PresenterMainGUI presenterMainGUI) {

		this.presenterMainGUI = presenterMainGUI;
		
	}

	@Override
	public void addMovieConfirm(String movieName, MovieStatus movieStatus) {
		
	presenterMainGUI.confirmAdd(movieName, movieStatus);
		
	}

	@Override
	public void delMovieConfirm(String movieName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void editMovieConfirm(String movieTitle, MovieStatus movieStatus,
			String oldTitle, MovieStatus oldStatus) {
		// TODO Auto-generated method stub
	
	}

	

}
