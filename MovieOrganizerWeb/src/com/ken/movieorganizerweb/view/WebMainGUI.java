package com.ken.movieorganizerweb.view;

import com.google.inject.Inject;
import com.ken.movieorganizer.MovieStatus;
import com.ken.movieorganizer.gui.presenter.PresenterMainGUI;
import com.ken.movieorganizer.gui.view.AddMovie;
import com.ken.movieorganizer.gui.view.ConfirmUpdate;
import com.ken.movieorganizer.gui.view.DeleteMovie;
import com.ken.movieorganizer.gui.view.EditChangeMovie;
import com.ken.movieorganizer.gui.view.EditSelectMovie;
import com.ken.movieorganizer.gui.view.MainGUI;
import com.ken.movieorganizer.storage.MovieStorage;
import com.ken.movieorganizerweb.controller.Controller;


public class WebMainGUI implements MainGUI {	
	
	
	PresenterMainGUI presenterMainGUI = new PresenterMainGUI(this);
	AddMovie addMovie;
	DeleteMovie deleteMovie;
	EditSelectMovie editSelectMovie;
	EditChangeMovie editChangeMovie;
	ConfirmUpdate confirmUpdate;
	Controller controller;
	
	@Inject
	public WebMainGUI(final AddMovie addMovie, final DeleteMovie deleteMovie, 
			final EditSelectMovie editSelectMovie, final EditChangeMovie 
			editChangeMovie, final ConfirmUpdate confirmUpdate, MovieStorage 
			movieStorage){	
		
		
		this.addMovie = addMovie;
		this.deleteMovie = deleteMovie;
		this.editSelectMovie = editSelectMovie;
		this.editChangeMovie = editChangeMovie;
		this.confirmUpdate = confirmUpdate;
		
		
		presenterMainGUI.setConfirmUpdate(confirmUpdate);
		presenterMainGUI.setMovieStorage(movieStorage);
		
		
	}
	
	
	
	
	public void addGame(String name, String status){
		
		addMovie.setNameAndStatus(name, status);
		
		presenterMainGUI.showAddMovieDialog(addMovie);
		
		
		
	}
	
	public void viewGameListJSON(){
		
		presenterMainGUI.getMovieListJSON();
		
	}	

	@Override
	public void createAndShowGUI() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void successAdd(String movieTitle, MovieStatus movieStatus) {

		controller.successAdd(movieTitle, movieStatus);
		
	}

	@Override
	public void noMovies() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void successEdit(String title, MovieStatus movieStatus,
			String oldTitle, MovieStatus oldStatus) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void appendMovies(String movieViewList) {


		controller.appendMovieList(movieViewList);
		
	}	

	@Override
	public void successDelete(String movieTitle, MovieStatus movieStatus) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void moviePreexists(String movieTitle) {
		
		controller.moviePreExistsCont(movieTitle);
		
	}

	@Override
	public void movieAndStatusPreexists(String movieTitle, MovieStatus oldStatus) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setController(Object controller) {

		this.controller = (Controller) controller;
		
	}
	
	@Override
	public void confirmAdd(String movieName, MovieStatus movieStatus) {


		controller.confirmAdd(movieName, movieStatus);
		
	}
	
	@Override
	public void addGameConfirmed() {
		
		Boolean confirm = true;
		presenterMainGUI.addConfirmResponse(confirm);
		
	}


}
