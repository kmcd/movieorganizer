package com.ken.movieorganizerweb.view;

import com.ken.movieorganizer.gui.presenter.PresenterAddMovie;
import com.ken.movieorganizer.gui.view.AddMovie;

public class WebAddMovie implements AddMovie{
	
	String name;
	String status;
	
	public void setNameAndStatus(String name, String status){
		
		this.name = name;
		this.status = status;
		
	}

	@Override
	public void dispose() {
		//Unnecessary in this implementation
		
	}

	@Override
	public void clearTextField() {
		//Unnecessary in this implementation
		
	}

	@Override
	public void showDialog(PresenterAddMovie presenterAddMovie) {


		presenterAddMovie.oKClicked(this.name, this.status);
		
		
	}

}
