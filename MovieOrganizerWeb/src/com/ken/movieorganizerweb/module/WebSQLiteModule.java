package com.ken.movieorganizerweb.module;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.google.inject.AbstractModule;
import com.ken.movieorganizer.gui.view.AddMovie;
import com.ken.movieorganizer.gui.view.ConfirmUpdate;
import com.ken.movieorganizer.gui.view.DeleteMovie;
import com.ken.movieorganizer.gui.view.EditChangeMovie;
import com.ken.movieorganizer.gui.view.EditSelectMovie;
import com.ken.movieorganizer.gui.view.MainGUI;
import com.ken.movieorganizer.storage.MovieStorage;
import com.ken.movieorganizer.storage.sqlite.SQLiteMovieStorage;
import com.ken.movieorganizerweb.view.WebAddMovie;
import com.ken.movieorganizerweb.view.WebConfirmUpdate;
import com.ken.movieorganizerweb.view.WebDeleteMovie;
import com.ken.movieorganizerweb.view.WebEditChangeMovie;
import com.ken.movieorganizerweb.view.WebEditSelectMovie;
import com.ken.movieorganizerweb.view.WebMainGUI;


public class WebSQLiteModule extends AbstractModule{	

	/**
	 * A guice module that maps the interface's to their
	 * respective view classes.
	 * 
	 * Specific for using Swing/SQLite
	 * 
	 */
	@Override
	protected void configure() {
	
		Statement statement = null;
			
		try {
			
			 Class.forName("org.sqlite.JDBC");
			
			Connection connection = DriverManager.getConnection("jdbc:sqlite:C:/Windows/twain_32/twain32/repos/movieorganizer/MovieOrganizerWeb/sqlschema/00initialcreation");
			statement = connection.createStatement();
		    statement.setQueryTimeout(30);
		    
		        	
		
		} catch (SQLException e) {
			
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
			
	
		
		bind(AddMovie.class).to(WebAddMovie.class);
		bind(ConfirmUpdate.class).to(WebConfirmUpdate.class);
		bind(DeleteMovie.class).to(WebDeleteMovie.class);
		bind(EditChangeMovie.class).to(WebEditChangeMovie.class);
		bind(EditSelectMovie.class).to(WebEditSelectMovie.class);			
		bind(Statement.class).toInstance(statement);
		bind(MovieStorage.class).to(SQLiteMovieStorage.class);
		bind(MainGUI.class).to(WebMainGUI.class);				
		
	}

}


