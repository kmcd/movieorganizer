

if ( !Date.prototype.toISOString ) {
  ( function() {
    
    function pad(number) {
      var r = String(number);
      if ( r.length === 1 ) {
        r = '0' + r;
      }
      return r;
    }
 
    Date.prototype.toISOString = function() {
      return this.getUTCFullYear()
        + '-' + pad( this.getUTCMonth() + 1 )
        + '-' + pad( this.getUTCDate() )
        + 'T' + pad( this.getUTCHours() )
        + ':' + pad( this.getUTCMinutes() )
        + ':' + pad( this.getUTCSeconds() )
        + '.' + String( (this.getUTCMilliseconds()/1000).toFixed(3) ).slice( 2, 5 )
        + 'Z';
    };
  
  }() );
}

/**
 * This function replaces the default jQuery.show() method with one that
 * fires a 'show' event. This allows us to attach event handlers to the
 * 'show' event.
 * 
 * @param {type} $
 * @returns {undefined}
 */
jQuery(function($) {
	var _oldShow = $.fn.show;
	
	$.fn.show = function(speed, oldCallback) {
		console.log('firing show');
		return $(this).each(function() {
			var obj = $(this);
			newCallback = function() {
				if($.isFunction(oldCallback)) {
					oldCallback.apply(obj);
				}
			};
			
			_oldShow.apply(obj, [speed, newCallback]);
			obj.trigger('show');
		});
	};
});

/**
 * This class takes a default tab to show for a specific page as a constructor
 * argument. It contains all of the core functions used for a generic page.
 * 
 * @param {type} defaultTab
 * @returns {undefined}
 */
var AjaxPage = function(defaultTab) {
	var self = this;
	
	self.myDefaultTab = defaultTab;
	
	/**
	 * 
	 * @param e A jQuery.Event instance.
	 * @returns void
	 */
	self.ajaxLinkClicked = function(e) {
		location.hash = this.parentNode.id.replace('-link', '');
		e.preventDefault();
	};
	
	/**
	 * Check the current location.hash value, change the related link to have
	 * class 'activeOption', hide unrelated sections and show the related
	 * section. Defaults to the 'home' section when there is no hash.
	 * 
	 * @returns void
	 */
	self.checkLocationHash = function() {
		var currentPage = window.location.hash.substr(1);
		// check for 'truthy' value, not empty string (in case we're null).
		if(!currentPage) {
			currentPage = self.myDefaultTab;
		}
		
		console.log(currentPage);
		$('.ajaxLink').parent().removeClass('activeOption');
		$('li[id=' + currentPage + '-link]').addClass('activeOption');
		$('.content-section').hide();
		$('#' + currentPage + '-content').show();
	};
	
	/**
	 * Bind a callback to a show event for a selector.
	 * 
	 * @param {type} selector
	 * @param {type} callback
	 * @returns {undefined}
	 */
	self.bindShowTrigger = function(selector, callback) {
		console.log('binding');
		$(selector).bind('show', callback);
	};
	
	/**
	 * Convieniance method to bind to a show event for a content section. For
	 * example: a section named 'home' will have an id of 'home-content', so
	 * its selector will be '#home-content'. This method simply needs the
	 * canonical name of the section - 'home' - and it will build the selector
	 * from there.
	 * 
	 * @param {type} sectionName
	 * @param {type} callback
	 * @returns {undefined}
	 */
	self.bindContentShow = function(sectionName, callback) {
		self.bindShowTrigger("#" + sectionName + "-content", callback);
	};
	
	/**
	 * 
	 * @returns {undefined}
	 */
	self.initialize = function() {
		$('.ajaxLink').click(self.ajaxLinkClicked);
		$(window).bind('hashchange', self.checkLocationHash);
		self.checkLocationHash();
	};
};


var common = new function() {
	
	var self = this;

	self.createHighlight = function (element) {

		element.addClass('ui-state-highlight ui-corner-all');
		element.html(
			'<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>'
			+ element.html()
			+ '</p>');
	};

	self.createError = function (element) {

		element.addClass('ui-state-error ui-corner-all');
		element.html(
			'<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>'
			+ element.html()
			+ '</p>');
	};

	self.initialize = function() {

		var element = $('div.errorMessage');
		if(element[0]) {
			Common.createError(element);
		}

		var element = $('div.infoMessage');
		if(element[0]) {
			Common.createHighlight(element);
		}
	};

	self.toLocalDate = function(utcDateString) {

        var nowLocal = new Date();
        var localTimezoneOffsetMilliseconds = nowLocal.getTimezoneOffset() * 60 * 1000;

        // "2013-07-02 21:23:11"
        var dateTimeParts = utcDateString.split(" ");
        var dateParts = dateTimeParts[0].split("-");
        var timeParts = dateTimeParts[1].split(":");

        // Months are zero based.
        var monthPart = dateParts[1] - 1;

        var utcDate = new Date(dateParts[0], monthPart, dateParts[2], timeParts[0], timeParts[1], timeParts[2]);
        return new Date(utcDate.getTime() - localTimezoneOffsetMilliseconds);
    };

  self.formatDate = function(dateString) {

    var value = new Date(dateString);
    var str = value.getMonth() + 1  + "/" + value.getDate() + "/" + value.getFullYear();
    value = null;

    return str;
  };

  self.toShortDateString = function(dateTime) {

      var day = dateTime.getDate();
      var month = (dateTime.getMonth() + 1);
      var year = dateTime.getFullYear();

      return  "" + month + "/" + day + "/" + year;
  };

  self.toDateString = function(dateTime) {

      var day = dateTime.getDate();
      var month = (dateTime.getMonth() + 1);
      var year = dateTime.getFullYear();
      var hour = dateTime.getHours();
      var minute = dateTime.getMinutes();
      var second = dateTime.getSeconds();

      return  "" + month + "/" + day + "/" + year + " " + hour + ":" + minute + ":" + second;
  };

  self.toUtcDateString = function(date) {

    return date.toISOString();
  };

  self.ellipsis = function(text, maxLength) {

      return text.length > maxLength ? text.substr(0, maxLength - 1) + '...' : text;
  };

};